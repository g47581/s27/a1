
const HTTP = require("http")
const PORT = 4000
HTTP.createServer((request, response) => {

    if (request.url === "/profile"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Welcome to your profile!");
        response.end()
    }  else if (request.url === "/courses"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Add a course to our resources");
        response.end();

    }else if (request.url === "/updatecourse"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Update a course to our resources");
        response.end();
    
    }else if (request.url === "/archivecourses"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Archive courses to our resources");
        response.end();
        
    
    }else{
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Welcome to Booking System");
        response.end();
    }

    
}).listen(PORT, () => console.log(`Server is connected to port ${PORT}`))